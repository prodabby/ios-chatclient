//
//  MessageCell.swift
//  ChatClient
//
//  Created by Dabby Ndubisi on 2015-04-21.
//  Copyright (c) 2015 Dabby Ndubisi. All rights reserved.
//

import UIKit

class MessageCell: UITableViewCell {
    
    @IBOutlet weak var senderLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!{
        didSet {
            messageLabel.numberOfLines = 10
            messageLabel.minimumScaleFactor = 0.5
        }
    }
    
    var message: Message? {
        didSet {
            updateUI()
        }
    }
    var messageColor: UIColor? {
        didSet {
            updateUI()
        }
    }
    
    
    
    func updateUI()
    {
        senderLabel.text = message?.username
        
        messageLabel.attributedText = NSAttributedString(string: message!.content, attributes: [NSForegroundColorAttributeName: messageColor ?? UIColor.blackColor()])
        
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

}
