//
//  Message.swift
//  ChatClient
//
//  Created by Dabby Ndubisi on 2015-04-21.
//  Copyright (c) 2015 Dabby Ndubisi. All rights reserved.
//

import Foundation

class Message {
    let username: String
    let content: String
    
    init(username: String, content: String){
        self.username = username
        self.content = content
    }
}