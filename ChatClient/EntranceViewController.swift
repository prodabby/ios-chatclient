//
//  EntranceViewController.swift
//  ChatClient
//
//  Created by Dabby Ndubisi on 2015-04-28.
//  Copyright (c) 2015 Dabby Ndubisi. All rights reserved.
//

import UIKit

class EntranceViewController: UIViewController, NSURLConnectionDelegate {
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    var username: String?
    var password: String?
    
    
    @IBAction func joinChat(sender: UIButton)
    {
        username = usernameField.text
        password = passwordField.text
        usernameField.resignFirstResponder()
        passwordField.resignFirstResponder()
        
        if(username != "" && password != ""){
            ChatClientModel.joinChat(username!, password: password!, delegate: self)
        }
    }
    
    struct Constants {
        static let segueId = "joinChat:"
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        var parseError: NSError?
        let parsedObject: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &parseError)
        //In response to success on POST request
        if let postedMessage = parsedObject as? NSDictionary {
            performSegueWithIdentifier(Constants.segueId, sender: nil)
        }
        else {
            //In response to failure on POST request.
            errorLabel.text = "Unable to Login For Given Username or Password"
            errorLabel.hidden = false

        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == Constants.segueId {
            var vc: UIViewController?
            if let navC = segue.destinationViewController as? UINavigationController {
                vc = navC.topViewController
            }
            
            if let dvc = vc as? ChatRoomViewController {
                dvc.currentUser = username!
                dvc.userColor = UIColor.random
            }
        }
    }
    
    @IBAction func logOut(sender: UIStoryboardSegue){
        usernameField.text = ""
        passwordField.text = ""
    }
}


private extension UIColor {
    class var random: UIColor {
        switch arc4random() % 5{
        case 0: return UIColor.greenColor()
        case 1: return UIColor.blueColor()
        case 2: return UIColor.orangeColor()
        case 3: return UIColor.redColor()
        case 4: return UIColor.purpleColor()
        default:
            return UIColor.blackColor()
        }
    }
}