//
//  ChatRoomViewController.swift
//  ChatClient
//
//  Created by Dabby Ndubisi on 2015-04-21.
//  Copyright (c) 2015 Dabby Ndubisi. All rights reserved.
//

import UIKit

class ChatRoomViewController: UITableViewController {
    @IBOutlet weak var messageField: UITextField!
    let model = ChatClientModel()
    var messages = [Message]()
    var currentUser: String?
    var userColor: UIColor?
    
    
    struct ChatRoomViewControllerConstants {
        static let messageCellIdentifier = "messageCell";
    }

    @IBAction func postMessage(sender: UIBarButtonItem) {
        if messageField.text != "" {
            model.postChatMessage(Message(username: currentUser!, content: messageField.text))
            messageField.text = ""
        }
        messageField.resignFirstResponder()
    }
    @IBAction func refresh(sender: UIRefreshControl) {
        model.getMessages()
        self.refreshControl?.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let center = NSNotificationCenter.defaultCenter()
        let queue = NSOperationQueue.mainQueue()
        center.addObserverForName(ChatClientModel.ChatClientModelConstants.messageDBChanged, object: model, queue: queue){notification in
            if let addedMessages = notification?.userInfo?[ChatClientModel.ChatClientModelConstants.changes] as? [Message] {
                addedMessages.map(){message in
                    self.messages.append(message)
                }
                self.tableView.reloadData()
            }
        }
        model.getMessages()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(ChatRoomViewControllerConstants.messageCellIdentifier, forIndexPath: indexPath) as! MessageCell

        // Configure the cell...
        cell.message = messages[messages.count - indexPath.row - 1]
        cell.messageColor = userColor
        
        return cell
    }
    
    

}