//
//  ChatClientModel.swift
//  ChatClient
//
//  Created by Dabby Ndubisi on 2015-04-21.
//  Copyright (c) 2015 Dabby Ndubisi. All rights reserved.
//

import Foundation

class ChatClientModel: NSObject, NSURLConnectionDataDelegate {
    var messages: [Message] = []
    
    struct ChatClientModelConstants {
        static let postMessageURL = "http://localhost:3000/users/postMessage"
        static let getMessagesURL = "http://localhost:3000/users/getMessages"
        static let joinChatURL = "http://localhost:3000/users/joinChat"
        static let messageDBChanged = "Messages Database Changed Notification"
        static let changes = "Changes"
    }
    
    func postChatMessage(message: Message){
        var urlReq = NSMutableURLRequest(URL: NSURL(string: ChatClientModelConstants.postMessageURL)!)
        urlReq.HTTPMethod = "POST"
        var messagePost = "username=\(message.username)&content=\(message.content)"
        if let data = messagePost.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false){
            urlReq.HTTPBody = data;
            let connection = NSURLConnection(request: urlReq, delegate: self)
            connection?.start()
        }
    }
    
    class func joinChat(username: String, password: String, delegate: NSURLConnectionDelegate?){
        var urlReq = NSMutableURLRequest(URL: NSURL(string: ChatClientModelConstants.joinChatURL)!)
        urlReq.HTTPMethod = "POST"
        var joinPost = "username=\(username)&password=\(password)"
        if let data = joinPost.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false){
            urlReq.HTTPBody = data;
            let connection = NSURLConnection(request: urlReq, delegate: delegate ?? self)
            connection?.start()
        }
        
    }
    
    func getMessages(){
        var urlReq = NSMutableURLRequest(URL: NSURL(string: ChatClientModelConstants.getMessagesURL)!)
        urlReq.HTTPMethod = "GET"
        let connection = NSURLConnection(request: urlReq, delegate: self)
        connection?.start()
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        var parseError: NSError?
        let parsedObject: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &parseError)
        //In response to POST request
        if let postedMessage = parsedObject as? NSDictionary {
            addToMessages(postedMessage, shouldPostNotification: true)
        }
        //In response to GET request
        if let receivedMessages = parsedObject as? NSArray {
            addToMessages(receivedMessages)
        }
    }
    
    private func addToMessages(jsonMessages: NSArray)
    {
        var addedMessages = [Message]()
        for (var i = messages.count; i < jsonMessages.count; i++){
            if let message = jsonMessages[i] as? NSDictionary {
                if let message = addToMessages(message, shouldPostNotification: false){
                    addedMessages.append(message)
                }
            }
        }
        if (addedMessages.count != 0){
            postNotificationForAddedMessages(addedMessages)
        }
    }
    
    private func addToMessages(jsonMessage: NSDictionary, shouldPostNotification: Bool) -> Message?{
        if let sender = jsonMessage["sender"] as? String{
            if let content = jsonMessage["content"] as? String{
                let message = Message(username: sender, content: content)
                messages.append(message)
                if(shouldPostNotification) {
                    postNotificationForAddedMessages([message])
                }
                return message
            }
        }
        return nil
    }
    
    private func postNotificationForAddedMessages(info: [Message]){
        let center = NSNotificationCenter.defaultCenter()
        let notification = NSNotification(name: ChatClientModelConstants.messageDBChanged, object: self, userInfo: [ChatClientModelConstants.changes: info])
        center.postNotification(notification)
    }
    
    
}